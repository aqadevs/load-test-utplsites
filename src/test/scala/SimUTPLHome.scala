import actions._
import io.gatling.core.Predef._
import io.gatling.core.session.Session
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration

import scala.concurrent.duration.FiniteDuration

import scala.concurrent.duration._

/**
 * GradebookTest, esta simulación se enfoca integra dos simulaciones anterios
 * : Authenticacion
 * : Descarga de archivos
 * : Carga de archivos
 */
class SimUTPLHome extends Simulation {

  val httpProtocol = http
    .inferHtmlResources(black = BlackList(""".*\.jpg""", """.*\.png"""))
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // 6
    .doNotTrackHeader("1")
    .inferHtmlResources() // simula el comportamiento del navegador al descargar todo los assets img, videos etc...
    //.inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .maxConnectionsPerHostLikeChrome
    .silentResources
    .disableWarmUp
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
    .maxRedirects(1)
    .shareConnections
//    .disableCaching


  val testGeneral = scenario("LOAD TEST UTPL")
      .exec(HomeUTPL())
//	.exec(ImageTest())
      //.exec(ServiciosUtpl())
      //.exec(CalendarioUTPL())
      //.exec(CanvasUTPL())
  
  setUp(
    testGeneral.inject(
//      atOnceUsers(5),
      //constantUsersPerSec(2) during(5)
//      constantConcurrentUsers(8) during(120)
      //rampUsers(9000) during (300),
      rampUsers(15000) during (5 minutes),
//      rampUsersPerSec(1) to 8 during (new FiniteDuration(60,TimeUnit.SECONDS)),
    )
  ).protocols(httpProtocol)

  // RPS TEST
    // setUp(
    // testGeneral.inject(
    //   constantUsersPerSec(8) during (new FiniteDuration(10,TimeUnit.SECONDS)) ,
    // ).throttle(reachRps(8) in (10))
    // ).protocols(httpProtocol)


}

