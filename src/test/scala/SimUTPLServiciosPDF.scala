import actions._
import io.gatling.core.Predef._
import io.gatling.core.session.Session
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration

import scala.concurrent.duration.FiniteDuration

/**
 * GradebookTest, esta simulación se enfoca integra dos simulaciones anterios
 * : Authenticacion
 * : Descarga de archivos
 * : Carga de archivos
 */
class SimUTPLServiciosPDF extends Simulation {

  val httpProtocol = http
    .inferHtmlResources(black = BlackList(""".*\.js""", """.*\.css"""))
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // 6
    .doNotTrackHeader("1")
    .inferHtmlResources() // simula el comportamiento del navegador al descargar todo los assets img, videos etc...
    .maxConnectionsPerHostLikeChrome
    .silentResources
    .disableWarmUp
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0")
    .maxRedirects(1)
    .disableCaching


  val testGeneral = scenario("LOAD TEST UTPL PDF")
      //.exec(ServiciosUtpl())
      .exec(DownloadPdf())
      .exec(HomeUTPL())
      .exec(session=>{
        val pdf = session("pdf").as[Array[Byte]]
        println(">>>> PDF SIZE BYTES " + pdf.length/(1024.0*1024.0) + "MB")
        session
      })
      //.exec(ServiciosUtpl())
      //.exec(CalendarioUTPL())
      //.exec(CanvasUTPL())
  
  setUp(
    testGeneral.inject(
      //atOnceUsers(50),
      //constantUsersPerSec(2) during(5)
      constantConcurrentUsers(20) during(20)
      //rampUsers(100) during (60),
      //rampUsers(5) during (120),
      //rampUsersPerSec(1) to 5 during (new FiniteDuration(25,TimeUnit.SECONDS)),
    )
  ).protocols(httpProtocol)

  // RPS TEST
    // setUp(
    // testGeneral.inject(
    //   constantUsersPerSec(8) during (new FiniteDuration(10,TimeUnit.SECONDS)) ,
    // ).throttle(reachRps(8) in (10))
    // ).protocols(httpProtocol)


}

