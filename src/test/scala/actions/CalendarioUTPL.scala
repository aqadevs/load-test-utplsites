package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object CalendarioUTPL {

  def apply() = {
    http("Calendario UTPL ")
      .get("https://servicios.utpl.edu.ec/")
      //.check(regex("""Gestión""").exists)
      //.check(status.is(s => 200))
      .check(status.not(404), status.not(500))
  }
}
