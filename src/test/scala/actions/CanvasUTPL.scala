package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object CanvasUTPL {

  def apply() = {
    http("Canvas UTPL")
      .get("https://www.utpl.edu.ec/canvas/")
      //.check(regex("""Calendario""").exists)
      //.check(status.is(s => 200))
      .check(status.not(404), status.not(500))
  }
}
