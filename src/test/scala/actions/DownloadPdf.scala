package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object DownloadPdf {

  def apply() = {
    http("Download PDF")
      .get("https://calendarioevaluaciones.utpl.edu.ec/links.pdf")
      .header("Content-Type", "application/pdf")
      .check(status.not(404), status.not(500))
      .check(status.is(s => 200))
      .check(bodyBytes.exists)
      .check(bodyBytes.is(RawFileBody("links.pdf")))
      .check(bodyBytes.saveAs("pdf")) 
  }
}
