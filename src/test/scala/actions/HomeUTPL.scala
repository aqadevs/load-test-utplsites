package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object HomeUTPL {

  def apply() = {
    http("Home UTPL www.utpl.edu.ec 20.36.184.227")
 //     .get("https://www.utpl.edu.ec")
	.get("http://20.36.184.227")
      .header("Content-Type", "application/xhtml+xml")
      .header("Connection", "Keep-Alive")
      .header("Keep-Alive", "timeout=15, max=300")
      .check(regex("""DECIDE SER MÁS""").exists)
      .check(status.not(404), status.not(500))
      .check(status.is(s => 200))
  }
}
