package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object ImageTest {

  def apply() = {
    http("Home UTPL www.utpl.edu.ec")
      .get("https://www.utpl.edu.ec/sites/default/files/becas.jpg")
	//.get("https://www.udla.edu.ec/wp-content/uploads/2020/05/1920x1080_WEB_CAMPA%C3%91A_VALORES_Mesa_de_trabajo_1_copia_2-scaled.jpg")
      //.header("Content-Type", "application/xhtml+xml")
      .header("Connection", "Keep-Alive")
      .header("Keep-Alive", "timeout=15, max=300")
      //.check(regex("""DECIDE SER MÁS""").exists)
      .check(status.not(404), status.not(500))
      .check(status.is(s => 200))
  }
}
