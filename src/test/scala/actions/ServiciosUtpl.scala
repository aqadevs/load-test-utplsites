package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object ServiciosUtpl {

  def apply() = {
    http("Servicios UTPL https://servicios.utpl.edu.ec/")
      .get("https://servicios.utpl.edu.ec/")
      .header("Content-Type", "application/xhtml+xml")
      .header("Connection", "Keep-Alive")
      .header("Keep-Alive", "timeout=15, max=500")
      //.check(regex("""Calendario""").exists)
      //.check(status.is(s => 200))
      .check(status.not(500))
      .check(status)
  }
}
