package actions

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object TestSite {

  def apply() = {
    http("Calendario UTPL ")
      .get("http://alcidestc.site/Decreto_No._1017_20200216213105.pdf")
      //.check(regex("""Gestión""").exists)
      //.check(status.is(s => 200))
      .check(status.not(404), status.not(500))
  }
}
